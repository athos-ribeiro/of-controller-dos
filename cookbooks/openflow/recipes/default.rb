package 'git'

execute 'pox:download' do
  command 'git clone http://github.com/noxrepo/pox'
  cwd '/home/vagrant'
  not_if { File.exist?('/home/vagrant/pox') }
end
