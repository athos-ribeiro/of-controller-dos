execute 'mininet:download' do
  command 'wget --no-check-certificate https://github.com/mininet/mininet/archive/2.2.1.zip'
  cwd '/home/vagrant'
  not_if { File.exist?('/usr/local/bin/mn') }
  notifies :run, 'execute[mininet:unzip]', :immediately
end

execute 'mininet:unzip' do
  command 'unzip 2.2.1.zip'
  cwd '/home/vagrant'
  action :nothing
  notifies :run, 'execute[mininet:rename]', :immediately
end

execute 'mininet:rename' do
  command 'mv mininet-2.2.1 mininet'
  cwd '/home/vagrant'
  action :nothing
  notifies :run, 'execute[mininet:install]', :immediately
end

execute 'mininet:install' do
  command 'mininet/util/install.sh -nfv'
  cwd '/home/vagrant'
  action :nothing
end

file '/home/vagrant/2.2.1.zip' do
  action :delete
end

execute 'xauthority-copy' do
  command 'rm /root/.Xauthority && cp /home/vagrant/.Xauthority /root/.Xauthority'
end
