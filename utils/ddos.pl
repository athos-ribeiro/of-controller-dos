use strict;
use warnings;
use IO::Socket;

my $sock = IO::Socket::INET->new(
  Proto    => 'udp',
  PeerPort => 5001,
  PeerAddr => '127.0.0.1',
) or die "Could not create socket: $!\n";

$sock->send("a file to have your advice\n") or die "Send error: $!\n";
